from flask import Flask, render_template, request, jsonify
import numpy as np
from keras.models import model_from_json
import base64
import cv2
import os

app = Flask(__name__)


# Get the directory of the current script (app.py)
script_dir = os.path.dirname(os.path.abspath(__file__))

# Construct the file path to the model JSON file
model_json_path = os.path.join(script_dir, "Model2.1", "model21.json")

# Load the machine learning model
with open(model_json_path, "r") as json_file:
    model_json = json_file.read()
model = model_from_json(model_json)
model.load_weights(os.path.join(script_dir, "Model2.1", "model21.h5"))

label_names = ['A', 'B', 'C', 'D', 'E', 'F', 'I', 'L', 'O', 'V', 'W', 'Y', 'nothing']

def add_background(image, bg_color=(255, 255, 255)):
    # Ensure the image is in the correct format
    if image.dtype != np.uint8:
        image = cv2.convertScaleAbs(image)
    
    # Assuming the input image is grayscale with a single channel
    if len(image.shape) == 3 and image.shape[2] == 1:
        image = image.squeeze(-1)
    
    # Create a white background
    background = np.ones_like(image) * 255
    
    # Convert the grayscale image to a binary mask
    _, mask = cv2.threshold(image, 60, 255, cv2.THRESH_BINARY_INV + cv2.THRESH_OTSU)
    
    # Invert the mask
    mask_inv = cv2.bitwise_not(mask)
    
    # Extract the hand region and combine with the background
    hand_region = cv2.bitwise_and(image, image, mask=mask)
    combined = cv2.bitwise_or(hand_region, mask_inv)
    
    return combined

def preprocess_frame(frame):
    frame = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)  # Convert image to grayscale
    frame = cv2.resize(frame, (48, 48))  # Resize image to 48x48
    frame = add_background(frame)  # Add white background
    feature = np.array(frame)
    feature = feature.reshape(1, 48, 48, 1)
    return feature / 255.0

@app.route('/')
def index():
    return render_template('index.html')

@app.route('/about')
def about():
    return render_template('about.html')

@app.route('/predict', methods=['POST'])
def predict():
    data_url = request.json['image']
    header, encoded = data_url.split(",", 1)
    data = base64.b64decode(encoded)
    nparr = np.frombuffer(data, np.uint8)
    frame = cv2.imdecode(nparr, cv2.IMREAD_COLOR)
    crop_frame = frame[40:300, 0:300]

    input_image_features = preprocess_frame(crop_frame)
    predictions = model.predict(input_image_features)
    predicted_label_index = np.argmax(predictions)
    predicted_label = label_names[predicted_label_index]
    prediction_accuracy = np.max(predictions) * 100

    return jsonify({
        'label': predicted_label,
        'accuracy': prediction_accuracy
    })

if __name__ == '__main__':
    # Run the app with Flask's built-in development server
    app.run(debug=True)