**HandSpeak**

**Overview**
This project implements a real-time American Sign Language (ASL) detection system through a webcam feed. The system utilizes machine learning techniques to recognize hand gestures and translate them into corresponding text or actions.

**Live Demo**
Access the live Sign Language Detection Website here: https://handspeak.onrender.com

**Purpose**
The primary objective of this project is to provide a user-friendly interface for real-time ASL detection, enabling communication for individuals who are deaf or hard of hearing, and also those wanting to learn sign language. The system aims to accurately interpret hand gestures and facilitate seamless interaction through the web interface.

**Features**
- Real-time ASL detection through webcam feed.
- Translation of detected signs into text.
- User-friendly interface for ease of interaction.
- Support for various ASL texts.

**Architecture**
The system utilizes a combination of computer vision and machine learning techniques for ASL detection:

- Image Acquisition: Capturing frames from the webcam feed.
- Hand Detection: Locating hand regions within the frames.
- Gesture Recognition: Utilizing a machine learning model (convolutional neural network) trained on ASL gestures to classify detected hand gestures.
- Output Generation: Translating recognized gestures into corresponding text and its accuracy.

**Functionalities**
- Users can interact with the system through the website
- ASL gestures captured by the webcam are instantly recognized and translated.
- Detected actions are translated and dsiplayed to the user in real-time.

**How to Use**
1. Clone this project : git clone url
2. Install Dependencies : pip install -r requirements.txt
3. Run the porject : python app.py
4. Allow webcam access if prompted and start using the ASL detection system.
